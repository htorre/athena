/*
  Copyright (C) 2002-2024 CERN for the benefit of the ATLAS collaboration
*/

#ifndef FPGATRACKSIMTRACKCOLLECTION_H
#define FPGATRACKSIMTRACKCOLLECTION_H

#include "AthContainers/DataVector.h"
#include "AthenaKernel/CLASS_DEF.h"
#include "FPGATrackSimObjects/FPGATrackSimTrack.h"


typedef std::vector<FPGATrackSimTrack> FPGATrackSimTrackCollection;
CLASS_DEF( FPGATrackSimTrackCollection , 1217492074 , 1 )

#endif // FPGATRACKSIMTRACKCOLLECTION_DEF_H
