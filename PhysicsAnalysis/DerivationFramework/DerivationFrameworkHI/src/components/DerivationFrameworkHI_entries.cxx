/*
  Copyright (C) 2002-2024 CERN for the benefit of the ATLAS collaboration
*/

#include "DerivationFrameworkHI/HITrackQualityAugmentationTool.h"
#include "../HITrackParticleThinningTool.h"
#include "../HIGlobalAugmentationTool.h"
#include "../HICentralityDecorationTool.h"

using namespace DerivationFramework;

DECLARE_COMPONENT( HITrackQualityAugmentationTool )
DECLARE_COMPONENT( HITrackParticleThinningTool )
DECLARE_COMPONENT( HIGlobalAugmentationTool )
DECLARE_COMPONENT( HICentralityDecorationTool )
