/*
  Copyright (C) 2002-2024 CERN for the benefit of the ATLAS collaboration
*/

#include "src/MeasurementToTrackParticleDecoration.h"
#include "src/PixelClusterTruthDecorator.h"
#include "src/StripClusterTruthDecorator.h"

DECLARE_COMPONENT(ActsTrk::MeasurementToTrackParticleDecoration)
DECLARE_COMPONENT(ActsTrk::PixelClusterTruthDecorator)
DECLARE_COMPONENT(ActsTrk::StripClusterTruthDecorator)
