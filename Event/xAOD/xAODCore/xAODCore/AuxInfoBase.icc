// Dear emacs, this is -*- c++ -*-
/*
  Copyright (C) 2002-2024 CERN for the benefit of the ATLAS collaboration
*/
#ifndef XAODCORE_AUXINFOBASE_ICC
#define XAODCORE_AUXINFOBASE_ICC

// System include(s):
#include <iostream>

// EDM include(s):
#include "AthContainers/AuxTypeRegistry.h"

// Local include(s):
#include "xAODCore/tools/AuxPersInfo.h"
#include "xAODCore/tools/AuxPersVector.h"
#include "xAODCore/tools/AuxVariable.h"

namespace xAOD {

   template< typename T >
   AuxInfoBase::auxid_t
   AuxInfoBase::getAuxID( const std::string& name,
                          T& /*info*/,
                          SG::AuxVarFlags flags,
                          const SG::auxid_t linkedVariable ) {

      return SG::AuxTypeRegistry::instance().template getAuxID< T >( name, "",
                                                                     flags,
                                                                     linkedVariable );
   }

   template< typename T >
   SG::IAuxTypeVector* makeVector( SG::auxid_t auxid, T& info, bool isLinked, SG::IAuxStore* store ) {
      if( isLinked ) {
         throw std::runtime_error( "Non-vector linked variable" );
      }
      return new AuxPersInfo< T >( auxid, info, store );
   }

   template< typename T >
   SG::IAuxTypeVector* makeVector( SG::auxid_t auxid, std::vector<T>& info, bool isLinked, SG::IAuxStore* store ) {
      if( isLinked ) {
         return new AuxPersVector< T >( auxid, info, true, store );
      }
      else {
         return new AuxPersInfo< std::vector<T> >( auxid, info, store );
      }
   }

   /// The user is expected to use this function inside the constructor of
   /// the derived class.
   ///
   /// @param auxid The auxiliary ID to use for the variable
   /// @param name The name of the variable. Same as the C++ variable's name.
   /// @param vec A reference to the auxiliary variable inside the object
   template< typename T >
   void AuxInfoBase::regAuxVar( const auxid_t auxid, const std::string& name,
                                T& info ) {

      // Make sure that the internal vector is big enough:
      if( m_vecs.size() <= auxid ) {
         m_vecs.resize( auxid + 1 );
      }

      // Check if this variable name was already registered:
      if( m_vecs[ auxid ] ) {
         std::cout << "WARNING xAOD::AuxInfoBase::regAuxVar "
                   << "Re-registering variable with name \""
                   << name.c_str() << "\"" << std::endl;
         delete m_vecs[ auxid ];
      }

      // Register the variable:
      const SG::AuxTypeRegistry& r = SG::AuxTypeRegistry::instance();
      m_vecs[ auxid ] = makeVector( auxid, info, r.isLinked( auxid ), this );

      // Remember that we are now handling this variable:
      m_auxids.insert( auxid );

      return;
   }

} // namespace xAOD

#endif // XAODCORE_AUXINFOBASE_ICC
